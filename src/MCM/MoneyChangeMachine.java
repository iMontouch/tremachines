package MCM;

public class MoneyChangeMachine {
    
    protected static double inputValue;
    protected static int [] notes;
    protected static int [] coins;
    protected static int [] ocoins;
    protected static double coinsValue;
    protected static int empty;
    
    public static String getAvailableCoins(){
        StringBuilder sb = new StringBuilder();
        double faceValue = 0;
        double totalValue = 0;
         
        double line;
        
        for (int i = 0 ;i <coins.length ; i++){
            switch(i){
                case 0: faceValue = 2;
                   break;
                case 1: faceValue = 1;
                    break;
                case 2: faceValue = 0.5;
                    break;
                case 3: faceValue = 0.2;
                    break;
                case 4: faceValue = 0.1;
                    break;
                case 5: faceValue = 0.05;
                    break;
                    
                
            }
                line = coins[i]*faceValue; 
            totalValue += line;
             sb.append("Eur: "+faceValue+coins[i]+line); 
                     sb.append("\n Total Value: " + totalValue);
                     return sb.toString();
        }
       
        
        return sb.toString();
    }

    
    
    public static void loadCoins(Coin type, int quantity) {
        switch (type) {
            case EUR_2:
                coins [0] += quantity;
                break;
            case EUR_1:
                coins [1]+= quantity;
                break;
            case CENT_50:
                coins [2] += quantity;
                break;
            case CENT_20:
                coins [3] += quantity;
                break;
            case CENT_10:
                coins [4] += quantity;
                break;
            case CENT_5:
                coins[5] += quantity;
                break;
            
            
                
        }
        
    }
    public static void loadNotes(Note type, int quantity) {
        switch (type) {
            case EUR_500:
                notes [0] += quantity;
                break;
            case EUR_200:
                notes [1]+= quantity;
                break;
            case EUR_100:
                notes [2] += quantity;
                break;
            case EUR_50:
                notes [3] += quantity;
                break;
            case EUR_20:
                notes [4] += quantity;
                break;
            case EUR_10:
               notes[5] += quantity;
                break;
            case EUR_5:
               notes[6] += quantity;
                break;
            
            
                
        }
        
    }
     public static void inputValue(Note type, int quantity) {
        switch (type) {
            case EUR_500:
                inputValue  += quantity*500;
                break;
            case EUR_200:
               inputValue  += quantity*200;
                break;
            case EUR_100:
                inputValue += quantity*100;
                break;
            case EUR_50:
                inputValue += quantity*50;
                break;
            case EUR_20:
                inputValue += quantity*20;
                break;
            case EUR_10:
               inputValue += quantity*10;
                break;
            case EUR_5:
               inputValue += quantity*5;
                break;
        }
     }
    
    public static void start() {

        
        
        
        
        notes = new int[7];

        notes [0] = 0;
        notes [1] = 0;
        notes [2] = 0;
        notes [3] = 0;
        notes [4] = 0;
        notes [5] = 0;
        notes [6] = 0;

        coins = new int[6];

        coins [0] = 1000;
        coins [1] = 1000;
        coins [2] = 1000;
        coins [3] = 1000;
        coins [4] = 1000;
        coins [5] = 1000;
        
        ocoins = new int [6];
        
        ocoins [0] = 0;
        ocoins [1] = 0;
        ocoins [2] = 0;
        ocoins [3] = 0;
        ocoins [4] = 0;
        ocoins [5] = 0;

        
        
        
    }
    static int getAvialbleCoins() {
        return 0;
    }
    public static int change() {
        
            
            ocoins[0] = 0;
            ocoins[1] = 0;
            ocoins[2]= 0;
            ocoins[3]= 0;
            ocoins[4]= 0;
            ocoins[5]= 0;
            
            // Bestand checken
            coinsValue = coins[0]*2+coins[1]*1+coins[2]*0.5+coins[3]*0.2+coins[4]*0.1+coins[5]*0.05;
            
            
            
            if (coinsValue>=inputValue){
                
            empty = 0;
            // 2€ ausgeben
            while (inputValue >= 2 && coins[0] > 0){
                inputValue -= 2;
                coins[0] -= 1;
                ocoins[0]++; // += 1; 
            }
            
            // 1€ ausgeben
            while (inputValue >= 1 && coins[1] > 0){
               inputValue -= 1;
               coins[1] -= 1;
               ocoins[1] += 1; 
            }
            // 0,5€ ausgeben
            while (inputValue >= 0.5 && coins[2] > 0){
               inputValue -= 0.5;
               coins[2] -= 1;
               ocoins[2] += 1; 
            }
            // 0,20€ ausgeben
            while (inputValue >= 0.2 && coins[3] > 0){
               inputValue -= 0.2;
               coins[3] -= 1;
               ocoins[3] += 1; 
            }
            // 0,10€ ausgeben
            while (inputValue >= 0.1 && coins[4] > 0){
               inputValue -= 0.1;
               coins[4] -= 1;
               ocoins[4] += 1; 
               
            }
            // 0.05€ ausgeben
            while (inputValue >= 2 && coins[5] > 0){
               inputValue -= 0.05;
               coins[5] -= 1;
               ocoins[5] += 1; 
               
               
            
            }
            
            }else{
                empty = 1;
               
            }
            return 0; 
        
   
        

    }
//    static int Inventory() {
//        int inventory = 0;
//        
//        
//        return inventory;
//    
//    
//    }    
}