//test256329562956
//test with netbeans
package MCM.test;

import MCM.MoneyChangeMachine;
import java.util.*;

public class MachineTest {
     public static void main(String[] args) {         
         MoneyChangeMachine.start();
         System.out.println("Willkommen zur Bank uns ");
         String again;
         
         do {
            System.out.println("Bitte Betriebsmodus wählen:");
            System.out.println("1. Geld wechseln");
            System.out.println("2. Geld nachfüllen");
            
            String modus;
            
            Scanner scanner9 = new Scanner(System.in);
            modus = scanner9.next();
            
            if (modus.equals("2")) {
                int coins0 = 0;
                int coins1 = 0;
                int coins2 = 0;
                int coins3 = 0;
                int coins4 = 0;
                int coins5 = 0;  
             
                boolean numbercheck0 = true;
                
                 do {
                    System.out.print("Anzahl 2€ Münzen: ");
                    Scanner scanner1 = new Scanner(System.in);
                    try {
                        coins0 = scanner1.nextInt();
                    } catch (InputMismatchException ex) {
                        System.out.println("PLX INSERT NUMBER");
                        numbercheck0 = false;
                    }
                 } while (numbercheck0 == false);
                 
             MoneyChangeMachine.loadCoins(MCM.Coin.EUR_2, coins0);
         
             boolean numbercheck1 = true;
             
             do {
                System.out.print("Anzahl 1€ Münzen: ");
                Scanner scanner1 = new Scanner(System.in);
                
                try {
                    coins1 = scanner1.nextInt();
                } catch (InputMismatchException ex) {
                    System.out.println("PLX INSERT NUMBER");
                    numbercheck1 = false;
                }
                
         } while (numbercheck1 == false);
         
         MoneyChangeMachine.loadCoins(MCM.Coin.EUR_1, coins1);
         
         boolean numbercheck2 = true;
         
         do {
            System.out.print("Anzahl 50ct Münzen: ");
            Scanner scanner1 = new Scanner(System.in);
            
            try {
                coins2 = scanner1.nextInt();
            } catch (InputMismatchException ex) {
                System.out.println("PLX INSERT NUMBER");
                numbercheck2 = false;
            }
         
         } while (numbercheck2 == false);
         
         MoneyChangeMachine.loadCoins(MCM.Coin.CENT_50, coins2);
         
         boolean numbercheck3 = true;
         do {
         System.out.print("Anzahl 20ct Münzen: ");
         Scanner scanner1 = new Scanner(System.in);
         try {
             coins3 = scanner1.nextInt();
         } catch (InputMismatchException ex) {
             System.out.println("PLX INSERT NUMBER");
             numbercheck3 = false;
         }
         } while (numbercheck3 == false);
            MoneyChangeMachine.loadCoins(MCM.Coin.CENT_20, coins3);
         
         boolean numbercheck4 = true;
         do {
         System.out.print("Anzahl 10ct Münzen: ");
         Scanner scanner1 = new Scanner(System.in);
         try {
             coins4 = scanner1.nextInt();
         } catch (InputMismatchException ex) {
             System.out.println("PLX INSERT NUMBER");
             numbercheck4 = false;
         }
         } while (numbercheck4 == false);
            MoneyChangeMachine.loadCoins(MCM.Coin.CENT_10, coins4);
         
         boolean numbercheck5 = true;
         do {
         System.out.print("Anzahl 5ct Münzen: ");
         Scanner scanner1 = new Scanner(System.in);
         try {
             coins5 = scanner1.nextInt();
         } catch (InputMismatchException ex) {
             System.out.println("PLX INSERT NUMBER");
             numbercheck5 = false;
         }
         } while (numbercheck5 == false);
            MoneyChangeMachine.loadCoins(MCM.Coin.CENT_5, coins5);
            
            System.out.println("Anzahl 2€: " + MoneyChangeMachine.coins[0]);
 
             
             
         }else{
         
         do{
         int notes0 = 0;
         int notes1 = 0;
         int notes2 = 0;
         int notes3 = 0;
         int notes4 = 0;
         int notes5 = 0;
         int notes6 = 0;

         
         System.out.println("Geldwecheln kann man machen.");
         
         boolean numbercheck0 = true;
         do {
            System.out.print("Anzahl 500€ Scheine: ");
            Scanner scanner1 = new Scanner(System.in);
            try {
                notes0 = scanner1.nextInt();
                numbercheck0 = true;
            } catch (InputMismatchException ex) {
                System.out.println("PLX INSERT NUMBER");
                numbercheck0 = false;
         }
         } while (numbercheck0 == false);
            MoneyChangeMachine.loadNotes(MCM.Note.EUR_500, notes0);
            MoneyChangeMachine.inputValue(MCM.Note.EUR_500, notes0);
         
         boolean numbercheck1 = true;
         do {
         System.out.print("Anzahl 200€ Scheine: ");
         Scanner scanner1 = new Scanner(System.in);
         try {
             notes1 = scanner1.nextInt();
             numbercheck1 = true;
         } catch (InputMismatchException ex) {
             System.out.println("PLX INSERT NUMBER");
             numbercheck1 = false;
         }
         } while (numbercheck1 == false);
            MoneyChangeMachine.loadNotes(MCM.Note.EUR_200, notes1);
            MoneyChangeMachine.inputValue(MCM.Note.EUR_200, notes1);
         
         boolean numbercheck2 = true;
         do {
         System.out.print("Anzahl 100€ Scheine: ");
         Scanner scanner1 = new Scanner(System.in);
         try {
             notes2 = scanner1.nextInt();
             numbercheck2 = true;
         } catch (InputMismatchException ex) {
             System.out.println("PLX INSERT NUMBER");
             numbercheck2 = false;
         }
         } while (numbercheck2 == false);
            MoneyChangeMachine.loadNotes(MCM.Note.EUR_100, notes2);
            MoneyChangeMachine.inputValue(MCM.Note.EUR_100, notes2);
         
         boolean numbercheck3 = true;
         do {
         System.out.print("Anzahl 50€ Scheine: ");
         Scanner scanner1 = new Scanner(System.in);
         try {
             notes3 = scanner1.nextInt();
             numbercheck3 = true;
         } catch (InputMismatchException ex) {
             System.out.println("PLX INSERT NUMBER");
             numbercheck3 = false;
         }
         } while (numbercheck3 == false);
            MoneyChangeMachine.loadNotes(MCM.Note.EUR_50, notes3);
            MoneyChangeMachine.inputValue(MCM.Note.EUR_50, notes3);
         
         boolean numbercheck4 = true;
         do {
         System.out.print("Anzahl 20€ Scheine: ");
         Scanner scanner1 = new Scanner(System.in);
         try {
             notes4 = scanner1.nextInt();
             numbercheck4 = true;
         } catch (InputMismatchException ex) {
             System.out.println("PLX INSERT NUMBER");
             numbercheck4 = false;
         }
         } while (numbercheck4 == false);
            MoneyChangeMachine.loadNotes(MCM.Note.EUR_20, notes4);
            MoneyChangeMachine.inputValue(MCM.Note.EUR_20, notes4);
         
         boolean numbercheck5 = true;
         do {
         System.out.print("Anzahl 10€ Scheine: ");
         Scanner scanner1 = new Scanner(System.in);
         try {
             notes5 = scanner1.nextInt();
             numbercheck5 = true;
         } catch (InputMismatchException ex) {
             System.out.println("PLX INSERT NUMBER");
             numbercheck5 = false;
         }
         } while (numbercheck5 == false);
            MoneyChangeMachine.loadNotes(MCM.Note.EUR_10, notes5);
            MoneyChangeMachine.inputValue(MCM.Note.EUR_10, notes5);
         
         boolean numbercheck6 = true;
         do {
         System.out.print("Anzahl 5€ Scheine: ");
         Scanner scanner1 = new Scanner(System.in);
         try {
             notes6 = scanner1.nextInt();
             numbercheck6 = true;
         } catch (InputMismatchException ex) {
             System.out.println("PLX INSERT NUMBER");
             numbercheck6 = false;
         }
         } while (numbercheck6 == false);
            MoneyChangeMachine.loadNotes(MCM.Note.EUR_5, notes6);
            MoneyChangeMachine.inputValue(MCM.Note.EUR_5, notes6);
            
        
            
        System.out.println("Eingabe gesamt: " +MoneyChangeMachine.inputValue);
        MoneyChangeMachine.change();
        
        if (MoneyChangeMachine.empty == 0){

        System.out.println("Anzahl 2€ Münzen: " +MoneyChangeMachine.ocoins[0]);
        System.out.println("Anzahl 1€ Münzen: " +MoneyChangeMachine.ocoins[1]);
        System.out.println("Anzahl 50ct Münzen: " +MoneyChangeMachine.ocoins[2]);
        System.out.println("Anzahl 20ct Münzen: " +MoneyChangeMachine.ocoins[3]);
        System.out.println("Anzahl 10ct Münzen: " +MoneyChangeMachine.ocoins[4]);
        System.out.println("Anzahl 5ct Münzen: " +MoneyChangeMachine.ocoins[5]);
        
       
        
        System.out.println("Noch mehr wechseln? (y/n)");
        Scanner scanner7 = new Scanner(System.in);
        again = scanner7.next();
        } else {
            System.out.println("Automat leer. Geld wird wieder ausgegeben.");
            again = "n";
        }
        
        
        
         } while (again.equals("y") || again.equals("Y"));
         }
         System.out.println("Zurück zum Hauptmenü? (y/n)");
         Scanner scanner10 = new Scanner(System.in);
        again = scanner10.next();
        } while (again.equals("y") || again.equals("Y"));
     
         
         System.out.println(MoneyChangeMachine.getAvailableCoins());
         
        
        
        
            
            
            
         
            
          //  System.out.println(MoneyChangeMachine.notes [5]);  Anzahl der Scheine für z.B 10€.
            
            
            

     }
}

